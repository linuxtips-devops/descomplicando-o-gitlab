# Descomplicando o GitLab

Treinamento Descomplicando o Gitlab criado ao vivo na Twich


### Day-1
```bash
- Entendemos o que é o Git.
- Entendemos o que é o Gitlab.
- Como criar um repositorio Git.
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no Git.
- Como criar um branch.
- Como criar um Merge Request.
- Como adicionar um Membro no projeto.
- Como fazer o merge na Master/Main
```